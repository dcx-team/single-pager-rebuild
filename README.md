# To Get Started:

## Project setup and Gulp installation
This project utilises open source components running on the Terminal/command-line for it's workflow, you'll need to install Node and Gulp. 

1.Install [Node.js] (https://nodejs.org/en/), [Sass] (http://sass-lang.com/tutorial.html) and [Git] (http://git-scm.com/) on your machine. If you're a Windows user you'll also need to install Ruby.

2.Install Gulp using npm install -g gulp. You may need to use sudo in front of the Gulp install command to give it permissions. 

3.Download [Sourcetree] (https://www.sourcetreeapp.com/) a visual Git client and signup with Monash credentials

4.Clone/Download this repo with git clone or look up the [SourceTree]  (https://confluence.atlassian.com/sourcetreekb/clone-a-repository-into-sourcetree-780870050.html) knowledge base. [https://bitbucket.org/dcx-team/single-pager/] (https://bitbucket.org/dcx-team/single-pager/)

5.In terminal type cd and drag the location of the folder into your terminal and hit enter.

6.When inside the directory which contains this project in terminal, type npm install. If (and only if) you're having trouble with npm install, try sudo npm install -- this makes you act as a super user

## Developing

In terminal type ‘gulp' and get developing, Gulp will report any errors with your code back to you on the command-line, even the line number. All CSS and JavaScript is uncompressed in development.

## Deploying

In Terminal enter 'gulp build' and your src directory files will be compiled into the bitbucket folder, but this time they're minified and ready to push onto Squiz Matrix.
````
├── bitbucket
│   ├── minified
│     ├── javasript
│     │   └── scripts.min.js
│     └── styles
│         ├── styles.min.css
│   ├── unminified
│     ├── javasript
│     │   └── behavs.js
│     │   └── Monash.M
│     │   └── Monash.Templates.OnePager.js
│     └── styles
│         ├── Monash.Bootstrap.css
│         ├── Monash.Forms.css
│         ├── Monash.Graphics.css
│         ├── Monash.Templates.css
├── localDev
│   ├── javasript
│   │   └── behavs.js
│   │   └── Monash.M
│   │   └── Monash.Templates.OnePager.js
│   └── styles
│       ├── Monash.Bootstrap.css
│       ├── Monash.Forms.css
│       ├── Monash.Graphics.css
│       ├── Monash.Templates.css
├── package.json
├── README.md
├── .gitignore
````

### testing bitbucket webhook