$(document).ready(function() {
  $("body").on("submit", ".nested-salesforce-form form", function(e) {
    var formComponent = $(this);

    if (!formComponent.hasClass("sending")) {
      var data = {},
        action = formComponent.attr("action"),
        errors = [];

      $.each(formComponent.find("input,textarea,select"), function() {
        var name = $(this).attr("name"),
          value = $(this)
            .find("option:selected")
            .val();

        if (!value) {
          value = $(this).val();
        }

        if ($(this).is(":visible")) {
          if (!value || value === "" || value === null) {
            var friendlyName = $(this)
              .prev("label")
              .text();
            errors.push(friendlyName + " is empty");
            $(this).addClass("form-field-error");
            $(this).addClass("form-field-error");

            $(this)
              .unbind("keyup")
              .bind("keyup", function(e) {
                var thisVal = $(this).val();
                if (thisVal.length > 0) {
                  $(this).removeClass("form-field-error");
                } else {
                  $(this).addClass("form-field-error");
                }
              });
          }
        }

        data[name] = value;
      });

      if (errors.length === 0) {
        formComponent.find("input[type=submit]").val("Sending...");
        formComponent.addClass("sending");

        $.ajax({
          type: "POST",
          url: action,
          data: data,
          headers: { "Access-Control-Allow-Origin": "https://www.monash.edu" },
          success: function(response) {
            formComponent.html(response);
          }
        });
      } else {
        alert("All fields are required.\n\n" + errors.join(", ") + ".");
      }
    }
    e.preventDefault();
  });

  if (/MSIE 10/i.test(navigator.userAgent)) {
    // This is internet explorer 10
    ieStyles();
  }

  if (
    /MSIE 9/i.test(navigator.userAgent) ||
    /rv:11.0/i.test(navigator.userAgent)
  ) {
    // This is internet explorer 9 or 11
    ieStyles();
  }

  if (/Edge\/\d./i.test(navigator.userAgent)) {
    // This is Microsoft Edge
    ieStyles();
  }

  function ieStyles() {
    var ieCSS =
      '<link href="https://www.monash.edu/__data/assets/css_file/0014/531023/inernet-explorer.css" rel="stylesheet" type="text/css" media="all"/>';
    $("head").append(ieCSS);
  }

  // Skip to content
  $(".skip-to-content").click(function(e) {
    var page = $(".page-container");
    page.attr("tabindex", "-1").focus();
    e.preventDefault();
  });

  $(".page-container").blur(function() {
    $(this).removeAttr("tabindex");
  });

  // Non-repeating delay function
  function typeDelay() {
    var timer = 0;
    return function(callback, ms) {
      clearTimeout(timer);
      timer = setTimeout(callback, ms);
    };
  }
  // throttle function, enforces a minimum time interval
  function throttle(fn, interval) {
    var lastCall, timeoutId;
    return function() {
      var now = new Date().getTime();
      if (lastCall && now < lastCall + interval) {
        // if we are inside the interval we wait
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
          lastCall = now;
          fn.call();
        }, interval - (now - lastCall));
        return "aborted";
      } else {
        // otherwise, we directly call the function
        lastCall = now;
        fn.call();
      }
    };
  }
  // Only run slideContentIn if we're not in an iframe
  if (window.top != window.self) {
    $(".load-in-ani > *").css({
      opacity: "1"
    });
  } else {
    $("body").slideContentIn(350, 200, null, 1);

    function slideContent() {
      if ($(".sly-container").length < 1) {
        $("body").slideContentIn(100, 125);
      }
    }
    $(window).scroll(throttle(slideContent, 500));
  }

  $("body").middleOfParent();
  $("body").adjustGalleries();
  $("body").scrollAssist();
  $("body").stickToHeader();
  $("body").backgroundVideoControls();
  $("body").iconPattern();

  // Issue fixes

  /*
        Issue where mobile background image fixed jumps on screen resize
    */

  var origHeight = $(window).height(),
    origWidth = $(window).width();

  $(window).resize(function() {
    $("body").middleOfParent();
    $("body").adjustGalleries();

    var fixedBackgrounds = $(".background-image.fixed.top");

    if ($(window).width() <= 1024) {
      var newHeight = $(window).height(),
        newWidth = $(window).width();

      // Scroll Down and hide toolbar
      if (origHeight !== newHeight && origWidth === newWidth) {
        fixedBackgrounds.css({
          top: -60 + (newHeight - origHeight),
          bottom: origHeight
        });
      }

      // Scroll Up and show toolbar
      if (origHeight === newHeight && origWidth === newWidth) {
        fixedBackgrounds.css({
          top: -60,
          height: origHeight + 60
        });
      }

      // Likely resize on desktop, or rotating device
      if (origHeight !== newHeight && origWidth !== newWidth) {
        fixedBackgrounds.css({
          top: 0,
          height: newHeight
        });
        origHeight = newHeight;
        origWidth = newWidth;
      }
    } else {
      fixedBackgrounds.css({
        width: "auto",
        height: "auto",
        top: 0,
        bottom: 0
      });
    }
  });

  $("body").on("click", "a.video-ui-toggle", function(e) {
    var thisVideo = $(this)
      .closest(".asset")
      .find("video.background-video");

    if ($(this).hasClass("playing")) {
      thisVideo[0].pause();
      $(this).removeClass("playing");
      $(this).addClass("paused");
    } else {
      thisVideo[0].play();
      $(this).addClass("playing");
      $(this).removeClass("paused");
    }
    e.preventDefault();
  });

  var slys = $(".standard-horizontal-sly");

  /*
    for(var i = 0; i < slys.length; i++){
        $(slys[i]).swipeMenu();
    } */

  $(slys).each(function() {
    $(this).swipeMenu();
  });

  // Start behavs

  $("body").progressiveLoad();

  // Find infographic elements and run them
  var infographics = $(".run-infographic");

  for (var i = 0; i < infographics.length; i++) {
    var $target = $(infographics[i]),
      type = $target.children(".infographic-type").val(),
      dataX = $target.children(".infographic-data-x").val(),
      dataY = $target.children(".infographic-data-y").val(),
      dataZ = $target.children(".infographic-data-Z").val(),
      dataArray = $target.children(".infographic-array").val();

    if (!type || type === "" || type === null) {
    } else {
      $target.graph({
        type: type,
        dataX: dataX,
        dataY: dataY,
        dataZ: dataZ,
        dataArray: dataArray
      });
    }
  }

  $("body").on("click", "a.play-video-button, div.play-video", function(e) {
    var target = $(this),
      container = target.closest(".video-container"),
      video = container.children("iframe"),
      overlay = container.children(".play-video"),
      src = video.attr("src") + "&autoplay=1";

    overlay.remove();

    video.attr("src", src);

    e.preventDefault();

    return false;
  });

  var tabs = $("ul.tabs");
  for (var i = 0; i < tabs.length; i++) {
    $(tabs[i]).tabList();
  }

  // Trigger resize to make any adjustments post initial CSS load
  $(window).resize();
});

(function($) {
  var zz = $("<i/>"),
    // Non-repeating delay function
    scrollTypeDelay = (function() {
      var timer = 0;
      return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
      };
    })();

  var scrollTop = 0,
    threshold = 30;

  $(window).scroll(function() {
    var thisScroll = 0;

    scrollTypeDelay(function() {
      thisScroll = $(window).scrollTop();

      if (thisScroll <= threshold) {
        adjustStickyHeader(0);
      } else {
        if (thisScroll > scrollTop) {
          if (thisScroll - scrollTop > threshold) {
            adjustStickyHeader(thisScroll, "down");
          }
        } else {
          if (scrollTop - thisScroll > threshold) {
            adjustStickyHeader(thisScroll, "up");
          }
        }
      }

      scrollTop = thisScroll;
    }, 15);

    $("body").progressiveLoad();
  });

  function adjustStickyHeader(scrollTop, direction) {
    var item = $("header"),
      itemHeight = item.outerHeight() + 1;

    if (scrollTop < 1) {
      item.css({
        transform: "translate3d(0px,0px,0px)",
        "-webkit-transform": "translate3d(0px,0px,0px)",
        "-moz-transform": "translate3d(0px,0px,0px)",
        "-ms-transform": "translate3d(0px,0px,0px)"
      });
    } else {
      if (scrollTop >= itemHeight) {
        item.css({
          transform: "translate3d(0px,-" + itemHeight + "px,0px)",
          "-webkit-transform": "translate3d(0px,-" + itemHeight + "px,0px)",
          "-moz-transform": "translate3d(0px,-" + itemHeight + "px,0px)",
          "-ms-transform": "translate3d(0px,-" + itemHeight + "px,0px)"
        });

        if (direction === "up") {
          item.css({
            transform: "translate3d(0px,0px,0px)",
            "-webkit-transform": "translate3d(0px,0px,0px)",
            "-moz-transform": "translate3d(0px,0px,0px)",
            "-ms-transform": "translate3d(0px,0px,0px)"
          });
        } else {
          item.css({
            transform: "translate3d(0px,-" + itemHeight + "px,0px)",
            "-webkit-transform": "translate3d(0px,-" + itemHeight + "px,0px)",
            "-moz-transform": "translate3d(0px,-" + itemHeight + "px,0px)",
            "-ms-transform": "translate3d(0px,-" + itemHeight + "px,0px)"
          });
        }
      } else {
      }
    }
  }

  $.fn.scrollAssist = function() {
    var target = $(this),
      ui = target.find(".return-to-top-navigation");

    if (zz.checkVariable(ui)) {
    } else {
      ui = $("<div/>").attr(
        "class",
        "return-to-top-navigation fixed bottom right small-padding transition"
      );

      var button = $("<a/>")
          .attr(
            "class",
            "inline-block default-trim small-round-corners padding go-to-top center"
          )
          .attr({
            href: "#",
            title: "Go to top"
          }),
        icon = zz.icon("caret-up", null, "small-padding");

      target.append(ui.append(button.append(icon)));

      button.click(function(e) {
        $("html, body")
          .stop(true, true)
          .animate(
            {
              scrollTop: 0
            },
            1000,
            "easeOutQuart"
          );

        e.preventDefault();
      });

      $(window).scroll(function() {
        var winHeight = $(window).height(),
          scroll = $(window).scrollTop();

        if (scroll >= winHeight * 1) {
          $("body").addClass("scrolled-down");
        } else {
          $("body").removeClass("scrolled-down");
        }
      });
    }
  };

  // Check Variables

  $.fn.checkVariable = function(check) {
    if (
      !check ||
      check === "" ||
      check === "undefined" ||
      check === null ||
      check === "null" ||
      check.length < 1
    ) {
      return false;
    } else {
      return true;
    }
  };

  // Get a new HTML element

  $.fn.element = function(elementType, classes) {
    var element = $("<" + elementType + "/>").attr(
      "class",
      "generated-ui " + classes
    );
    return element;
  };

  // Get an icon

  $.fn.icon = function(name, family, classes) {
    if (zz.checkVariable(family)) {
    } else {
      // Default to Font Awesome
      family = "fa fa-";
    }
    if (zz.checkVariable(classes)) {
    } else {
      // Default to Font Awesome
      classes = "";
    }

    return $("<i/>").attr("class", family + name + " " + classes);
  };

  // Animate element in via CSS

  $.fn.introduce = function(delay, anchorPoint) {
    var element = $(this);

    if (zz.checkVariable(delay)) {
    } else {
      delay = 500;
    }

    if (zz.checkVariable(anchorPoint)) {
    } else {
      anchorPoint = "top";
    }

    var offset = 20;

    if (anchorPoint === "top") {
      offset = 0 - offset;
    }

    element.css(anchorPoint, offset + "em");
    element.css("opacity", 0);
    element.show();

    setTimeout(function() {
      element.addClass("short-transition");
      element.removeAttr("style");
    }, delay);
  };

  // Set to same height as window, minus fixed header

  $.fn.matchWindowHeight = function() {
    var winHeight = $(window).height() - $("header").outerHeight();
    $(this).css("height", winHeight);
  };

  $.fn.backgroundVideoControls = function() {
    var target = $(this),
      backgroundVideos = target.find("video.background-video");

    for (var i = 0; i < backgroundVideos.length; i++) {
      var thisVideo = $(backgroundVideos[i]),
        videoID = $(thisVideo).attr("id");

      if (thisVideo.hasClass("ui-added")) {
      } else {
        var uiContainer = $("<div/>").attr(
            "class",
            "absolute bottom right top-layer desktop-only"
          ),
          uiInner = $("<div/>").attr("class", "padding"),
          uiToggle = $("<a/>")
            .attr(
              "class",
              "video-ui-toggle white pointer playing large text-shadow"
            )
            .attr({
              href: "#",
              title: "Play/Pause background video",
              "aria-controls": videoID
            }),
          uiIcons = [
            zz.icon("play", null, "play-video-icon"),
            zz.icon("pause", null, "pause-video-icon")
          ];

        var parentAssetContainer = $(thisVideo).closest(".asset");

        parentAssetContainer
          .addClass("layer-1")
          .append(uiContainer.append(uiInner.append(uiToggle.append(uiIcons))));

        thisVideo.addClass("ui-added");
      }
    }
  };

  $.fn.progressiveLoad = function() {
    var target = this,
      items = target.find(".background-image,.background-video"),
      winHeight = $(window).height() - $("header").outerHeight(),
      winScroll = $(window).scrollTop(),
      load = false;

    for (var i = 0; i < items.length; i++) {
      var $target = $(items[i]),
        scrollY = $target.offset().top;

      if ($target.hasClass("fixed")) {
        scrollY = $target.closest(".relative").offset().top;
      }

      if (scrollY < winHeight * 2 + winScroll && !$target.hasClass("loaded")) {
        if ($target.is("div")) {
          var style = $target.attr("data-background");
          $target.attr("style", style);
        }

        if ($target.is("video")) {
          $target[0].play();
          console.log("Video loaded");
        }

        $target.addClass("loaded");
      }
    }
  };

  // Makes a horizontal list menu swipable

  $.fn.swipeMenu = function(sly, index, reset, pageThumbs) {
    if (!index) {
      index = 0;
    }

    var $frame = $(this),
      $parent = $frame.parent(),
      $links = $frame.find("a"),
      $tabbable = $frame.find("*[tabindex]"),
      slides = $frame.children("ul").children("li"),
      helper = zz.element(
        "div",
        "absolute top right bottom left no-click swipe-menu-helper"
      ),
      options = {
        smart: true,
        itemNav: "basic",
        startAt: index,
        speed: 300,
        easing: "easeOutExpo",
        swingSpeed: 0.2,
        horizontal: true,
        scrollSource: $parent,
        scrollBy: 0,
        scrollBar: $parent.find(".scrollbar"),
        activateOn: "click",
        activatePageOn: "click",
        elasticBounds: 1,
        touchDragging: 1,
        releaseSwing: 1,
        mouseDragging: 1,
        dragHandle: 1,
        dynamicHandle: 1,
        clickBar: 1,
        interactive: "[data-editable][data-field]",
        prev: $parent.closest(".asset").find("a.sly-prev-item"),
        next: $parent.closest(".asset").find("a.sly-next-item"),
        prevPage: $parent.closest(".asset").find("a.sly-prev"),
        nextPage: $parent.closest(".asset").find("a.sly-next"),
        pagesBar: $parent.closest(".asset").find(".pages-nav"),
        pageBuilder: function(index) {
          var pageTabContent = "",
            pageTabClass = "";

          if (!pageThumbs) {
          } else {
            pageTabContent =
              '<img class="full-height" src="' + pageThumbs[index] + '">';
            pageTabClass = "row-list-thumbnail small-padding no-padding-bottom";
          }

          var pageTab =
            '<li class="inline-block pointer ' +
            pageTabClass +
            '" aria-label="Go to item ' +
            (index + 1) +
            '">' +
            pageTabContent +
            "</li>";
          return pageTab;
        }
      };

    if (
      $parent.children(".swipe-menu-helper").length < 1 &&
      $frame.hasClass("helper")
    ) {
      $parent.append(helper);
    }

    if (!$frame.attr("data-interval")) {
    } else {
      options["speed"] = 1000;
      options["cycleBy"] = "items";
      options["cycleInterval"] = $frame.attr("data-interval") + "000";
      options["pauseOnHover"] = true;
    }

    if ($frame.hasClass("full-width-sly")) {
      for (var i = 0; i < slides.length; i++) {
        $(slides[i]).css({
          width: $frame.width()
        });
      }
    } else {
      for (var i = 0; i < slides.length; i++) {
        $(slides[i]).css({
          width: "auto",
          height: "auto"
        });
        var width = $(slides[i]).outerWidth();

        //   $(slides[i]).css('width',width);
      }
    }

    if (!sly || sly === null) {
      sly = new Sly($frame, options);

      sly.on("moveStart", function() {});

      sly.on("moveEnd", function() {});

      sly.on("cycle", function() {});

      sly.on("activate", function() {
        console.log("activate");
      });

      $links.attr("aria-selected", "false");

      slides.focus(function(e) {
        var target = $(this),
          link = target.find("a").eq(0);

        link.focus();
        console.log("Slide focus");
        e.preventDefault();
      });

      $tabbable.focus(function() {
        var target = $(this),
          slide = target.closest("li"),
          index = slide.index();

        $frame.closest(".transparent").addClass("opaque");

        sly.activate(index);
      });

      $links.focus(function(e) {
        var target = $(this),
          slide = target.closest("li"),
          index = slide.index();

        $frame.closest(".transparent").addClass("opaque");

        sly.activate(index);

        // e.preventDefault();
      });

      // Keyboard navigation for tabs
      $links.keyup(function(e) {
        var keyCode = event.keyCode ? event.keyCode : event.which;

        if (keyCode == 13) {
          var target = $(this),
            relation = target.attr("aria-controls");

          if (zz.checkVariable(relation)) {
            $("#" + relation)
              .attr("tabindex", -1)
              .focus();
          }

          e.preventDefault();
        }
      });

      sly.init();

      var slyExpanded = 0;

      $(window).resize(function() {
        if ($frame.hasClass("full-width-sly")) {
          for (var i = 0; i < slides.length; i++) {
            $(slides[i]).css({
              width: $frame.width()
            });
          }
        } else {
          for (var i = 0; i < slides.length; i++) {
            $(slides[i]).css({
              width: "auto",
              height: "auto"
            });
            var width = $(slides[i]).outerWidth();

            //   $(slides[i]).css('width',width);
          }
        }

        sly.reload();

        if (
          $($frame)
            .closest(".master-wrap")
            .width() < $frame.children("ul").outerWidth() &&
          !$frame.hasClass("standard-horizontal-sly")
        ) {
          if (slyExpanded === 0) {
            helper.show();
            $($frame)
              .closest(".master-wrap")
              .addClass("initial");
          }
        } else {
          if (slyExpanded === 0) {
            helper.hide();
            $($frame)
              .closest(".master-wrap")
              .removeClass("initial");
          }
        }
      });

      if (
        $($frame)
          .closest(".master-wrap")
          .width() < $frame.children("ul").outerWidth() &&
        !$frame.hasClass("standard-horizontal-sly")
      ) {
        helper.show();
        $($frame)
          .closest(".master-wrap")
          .addClass("initial");
        slyExpanded = 1;
        setTimeout(function() {
          $(window).resize();
        }, 500);
      } else {
        helper.hide();
        $($frame)
          .closest(".master-wrap")
          .removeClass("initial");
      }

      return sly;
    } else {
      sly.activate(index);
      slides.find("a").attr("aria-selected", "false");
      slides
        .eq(index)
        .find("a")
        .attr("aria-selected", "true");

      /* if($frame.hasClass('full-width-sly')){
                slides.attr('aria-hidden','false');
                slides.eq(index).attr('aria-hidden','true');
            } */
    }
  };

  // Generate infographic graphs

  $.fn.graph = function(options) {
    var settings = $.extend(
        {
          type: null,
          dataX: null,
          dataY: null,
          dataZ: null,
          dataArray: []
        },
        options
      ),
      target = this,
      shell = $("<div/>").attr("class", "padding infographic"),
      percent = settings["dataX"] / settings["dataY"];

    // Circles

    if (settings.type === "Circle") {
      target.append(shell);

      var infographic = new ProgressBar.Circle(shell, {
        strokeWidth: 10,
        easing: "easeInOut",
        duration: 1400,
        color: "#FFF",
        trailColor: "#FFF",
        trailWidth: 0.5,
        svgStyle: null,
        text: "Test"
      });

      infographic.animate(percent);
    }
  };

  $.fn.tabList = function(goTo) {
    var target = $(this),
      parent = target.parent(),
      menu = parent.find("ul.tab-menu"),
      tabs = target.children("li.tab"),
      menuButtons = [],
      labels = {};

    if (zz.checkVariable(goTo)) {
    } else {
      goTo = 0;
    }

    for (var i = 0; i < tabs.length; i++) {
      var item = $(tabs[i]),
        name = item.attr("data-name"),
        id = item.attr("data-asset"),
        li = $('<li class="inline-block"/>'),
        anchor = $(
          '<a class="block tab-menu-button pointer bold no-underline default-untrim"/>'
        ).attr("href", "#"),
        label = $('<div class="padding system"/>'),
        text = $('<span class="block"/>')
          .attr({
            "data-editable": "true",
            "data-attribute": "name",
            "data-save-to": id
          })
          .html(name);

      labels[i] = anchor;

      if (item.index() === goTo) {
        item.show();
        anchor.addClass("default-trim");
        anchor.removeClass("default-untrim");
      } else {
        item.hide();
        anchor.removeClass("default-trim");
      }

      anchor.click(function(e) {
        var thisIndex = $(this)
            .parent("li")
            .index(),
          tab = tabs.eq(thisIndex);

        menu
          .find(".default-trim")
          .removeClass("default-trim")
          .addClass("default-untrim");

        $(labels[thisIndex])
          .removeClass("default-untrim")
          .addClass("default-trim");

        tabs.hide();
        tab.show();

        e.preventDefault();
      });

      menuButtons.push(li.append(anchor.append(label.append(text))));
    }

    menu.empty();
    menu.append(menuButtons);
  };

  $.fn.middleOfParent = function() {
    var middleOfParent = $(".always-middle-of-parent");

    for (var i = 0; i < middleOfParent.length; i++) {
      var $target = $(middleOfParent[i]),
        $parent = $target.closest(".relative"),
        hidden = false;

      if (
        $target.parent().hasClass("absolute") ||
        $target.parent().hasClass("fixed")
      ) {
        $parent = $target.parent();
      }

      if ($target.hasClass("hidden")) {
        hidden = true;
        $target.removeClass("hidden");
      }

      var targetX = $target.outerWidth(),
        targetY = $target.outerHeight(),
        parentX = $parent.outerWidth(),
        parentY = $parent.outerHeight();

      $target.css({
        top: parentY / 2 - targetY / 2,
        left: parentX / 2 - targetX / 2
      });

      if (hidden === true) {
        $target.addClass("hidden");
      }
    }
  };

  $.fn.adjustGalleries = function() {
    var galleryItems = $(".gallery-item");

    for (var i = 0; i < galleryItems.length; i++) {
      var target = $(galleryItems[i]),
        img = target.children("img"),
        width = img.attr("data-width"),
        height = img.attr("data-height"),
        X = img.outerWidth(),
        Y = img.outerHeight(),
        ratio = width / height,
        newWidth = Y * ratio;

      img.css("width", newWidth);
    }
  };

  $.fn.iconPattern = function() {
    $(this)
      .find(".icon-tesselation")
      .each(function() {
        var target = $(this),
          iconName = target.attr("data-icon"),
          targetWidth = target.outerWidth(),
          targetHeight = target.outerHeight(),
          horizCount = (targetWidth / 15).toFixed(0),
          sizeClasses = [
            "large-headline",
            "headline",
            "giant-headline",
            "largerer",
            "large",
            "smaller"
          ],
          opacityClasses = ["semi-transparent", "almost-transparent", "opaque"],
          styleClasses = ["black", "untrim", "stroke", "white"];

        target.addClass("center");

        for (var i = 0; i < 100; i++) {
          var appendIconWrap = $("<div/>").attr(
              "class",
              "inline-block padding center"
            ),
            appendIcon = $("<i/>").attr("class", "rotate-45 fa fa-" + iconName),
            randomSize = sizeClasses[Math.floor(Math.random() * 5 + 0)],
            randomOpacity = opacityClasses[Math.floor(Math.random() * 2 + 0)],
            randomStyle = styleClasses[Math.floor(Math.random() * 3 + 0)];

          appendIconWrap
            .addClass(randomSize)
            .addClass(randomOpacity)
            .addClass(randomStyle);

          target.append(appendIconWrap.append(appendIcon));
        }
      });
  };

  $.fn.stickToHeader = function() {
    $(".stick-to-header.stuck").remove();
    $(".stick-to-header-placeholder").remove();

    var stickToHeaders = $(".stick-to-header");

    stickToHeaders.each(function() {
      var $target = $(this),
        scrollY = $target.attr("data-unstick"),
        height = $target.outerHeight(),
        clone;

      $(window).scroll(function() {
        var thisScroll = $(window).scrollTop();

        if (!scrollY) {
          scrollY = $target.offset().top;
        }

        if (thisScroll >= scrollY - height) {
          if (!$target.hasClass("stuck")) {
            clone = $("<div/>")
              .attr("class", "stick-to-header-placeholder")
              .css("height", height);

            $target.after(clone);
            $target.appendTo(".header-stickies");
            $target.attr("data-unstick", scrollY);
            $target.addClass("stuck");
          }
        } else {
          if ($target.hasClass("stuck")) {
            $target.removeClass("stuck");
            clone.after($target);
            clone.remove();
          }
        }
      });
    });
  };

  /* Animate content as user scrolls down page */

  $.fn.slideContentIn = function(
    loadInTimer,
    increment,
    element,
    threshOverride
  ) {
    // Disable load in animation on most tablets/phones
    if ($(window).width() <= 1024) {
      $(".load-in-ani > *").css({
        opacity: 1
      });
    } else {
      if (!threshOverride) {
        threshOverride = 100;
      }

      var winHeight = $(window).height(),
        scroll = $(window).scrollTop(),
        thresh = winHeight + scroll - threshOverride,
        targets;

      if (!element || element === null || element.length < 1) {
        targets = $(".load-in-ani > *");
      } else {
        targets = element;
      }

      targets.each(function() {
        var target = $(this);

        target.css({ transition: "initial" });

        var currentBtm = $(this).css("bottom"),
          currentHeight = $(this).outerHeight(),
          offsetTop = $(this).offset().top,
          preStoredBtm = $(this).attr("data-set-btm");

        if (!preStoredBtm || preStoredBtm === null || preStoredBtm === "") {
          preStoredBtm = currentBtm;
          $(this).attr("data-set-btm", preStoredBtm);
        }

        if (!element || element === null || element.length < 1) {
          if (target.hasClass("slided")) {
          } else {
            if (offsetTop < scroll) {
              target.addClass("slided");
              target.css({
                opacity: 1
              });
            } else {
              if (offsetTop < thresh) {
                target.addClass("slided");

                if (
                  !preStoredBtm ||
                  preStoredBtm === "auto" ||
                  preStoredBtm === null ||
                  preStoredBtm === "initial"
                ) {
                  preStoredBtm = 0;
                }

                var targetPos = target.css("position");
                if (
                  !targetPos ||
                  targetPos === null ||
                  targetPos === "" ||
                  targetPos === "static"
                ) {
                  targetPos = "relative";
                }

                target.css({
                  opacity: 0,
                  transform: "translate3d(0px,50px,0px)",
                  "-webkit-transform": "translate3d(0px,50px,0px)",
                  "-moz-transform": "translate3d(0px,50px,0px)"
                });

                setTimeout(function() {
                  target.css({
                    transition: ".8s cubic-bezier(0.075, 0.820, 0.165, 1.000)",
                    opacity: 1,
                    transform: "translate3d(0px," + preStoredBtm + ",0px)",
                    "-wbkit-transform":
                      "translate3d(0px," + preStoredBtm + ",0px)",
                    "-moz-transform":
                      "translate3d(0px," + preStoredBtm + ",0px)"
                  });
                }, loadInTimer);

                loadInTimer = loadInTimer + increment;
              }
            }
          }
        } else {
          var targetPos = target.css("position");
          if (
            !targetPos ||
            targetPos === null ||
            targetPos === "" ||
            targetPos === "static"
          ) {
            targetPos = "relative";
          }

          target.css({
            opacity: 0,
            transform: "translate3d(0px,50px,0px)",
            "-webkit-transform": "translate3d(0px,50px,0px)",
            "-moz-transform": "translate3d(0px,50px,0px)"
          });

          setTimeout(function() {
            target.css({
              transition: ".8s cubic-bezier(0.075, 0.820, 0.165, 1.000)",
              opacity: 1,
              transform: "translate3d(0px," + preStoredBtm + ",0px)",
              "-wbkit-transform": "translate3d(0px," + preStoredBtm + ",0px)",
              "-moz-transform": "translate3d(0px," + preStoredBtm + ",0px)"
            });

            target.find(".typed").each(function() {
              var text = $(this).text();
              $(this).typed({
                strings: [text],
                typeSpeed: -5,
                showCursor: false
              });
            });

            setTimeout(function() {
              target.css({ transition: "initial" });
            }, loadInTimer);
          }, loadInTimer);

          loadInTimer = loadInTimer + increment;
        }
      });
    }
  };

  $.fn.closeBtn = function() {
    var container = zz.element(
        "div",
        "inline-block close-button padding mobile-large"
      ),
      crossA = zz.element(
        "div",
        "bar border-bottom rotate-45 transition absolute left-25 half-width top-50 mobile-half-width"
      ),
      crossB = zz.element(
        "div",
        "bar border-bottom rotate--45 transition absolute left-25 half-width top-50 mobile-half-width"
      );

    return container.append(crossA).append(crossB);
  };

  $.fn.toggleFullScreen = function() {
    if (
      !document.fullscreenElement &&
      !document.mozFullScreenElement &&
      !document.webkitFullscreenElement &&
      !document.msFullscreenElement
    ) {
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.msRequestFullscreen) {
        document.documentElement.msRequestFullscreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(
          Element.ALLOW_KEYBOARD_INPUT
        );
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }
  };
})(jQuery);
