(function($) {

    var zz = $('<i/>'),

        // Non-repeating delay function
        typeDelay = (function() {
            var timer = 0;
            return function(callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

    function closestNumber(array, num) {
        var i = 0;
        var minDiff = 1000;
        var ans;
        for (i in array) {
            var m = Math.abs(num - array[i]);
            if (m < minDiff) {
                minDiff = m;
                ans = array[i];
            }
        }
        return ans;
    }

    $.fn.onePager = function(command) {
        var target = $(this),
            pages,
            menuStyle;

        if (zz.checkVariable(target)) {

            pages = target.children('.asset');

            menuStyle = target.attr('data-menu-style');

            if (zz.checkVariable(pages)) {

                // INITIATE
                if (command === 'init') {

                    target.find('.generated-ui').remove();


                    var asset = target.attr('data-asset'),
                        layout = target.attr('data-result-layout'),
                        nextTab = zz.element('div', 'absolute right bottom left large-padding center only-if-active'),
                        nextLink = zz.element('a', 'one-pager-next-links circle inline-block larger white no-underline').attr('href', '#'),
                        nextIcon = zz.icon('angle-down'),

                        // Menu elements

                        pagesMenuWrap = zz.element('div', 'page-menu-wrap helper overflow-initial'),
                        pagesMenu = zz.element('ul', 'no-list list-menu no-wrap one-pager-menu').attr({
                            'role': 'tablist',
                            'data-asset': asset,
                            'data-result-layout': layout
                        }),
                        pagesMenuOptions = [];

                    nextTab.append(
                        nextLink.append(
                            nextIcon
                        )
                    );

                    // Page generation

                    for (var i = 0; i < pages.length; i++) {

                        // Next page button

                        if ($(pages[i]).hasClass('window-height')) {
                            $(pages[i]).matchWindowHeight();
                        }

                        var thisNextTab = nextTab.clone();

                        if (i < pages.length - 1) {
                            //    $(pages[i]).append(thisNextTab);
                            //    thisNextTab.introduce(1000,'bottom');
                        }

                        // Menu links

                        var count = i + 1;




                        if (i === 0) {
                            // anchor.addClass('active untrim white-background');
                        }

                        var nextPageLink = $(pages[i]).find('a.one-pager-next-links'),
                            nextPage = $(pages[i + 1]);

                        if (zz.checkVariable(nextPage)) {

                            var nextPageLabel = nextPage.attr('data-name'),
                                nextPageID = nextPage.attr('id');

                            nextPageLink.children('span').html(nextPageLabel);
                            nextPageLink.show();

                            nextPageLink.attr({
                                'aria-controls': 'page-section-' + (i + 2)
                            });
                        } else {
                            nextPageLink.hide();
                        }
                    }

                    pagesMenuWrap.append(
                        pagesMenu.append(
                            pagesMenuOptions
                        )
                    );

                    if (menuStyle === "Always on Top") {
                        pagesMenuWrap.addClass('relative top').appendTo('.sticky-nav-tracks.one-pager-nav');
                        pagesMenu.children('li');
                    }

                    var heightBuffer = zz.element('div', 'sticky-nav-height-buffer no-desktop relative top-layer white-background').css('height', pagesMenuWrap.outerHeight() - 1);
                    // $(pages[0]).prepend(heightBuffer);

                    var stuck = 0,
                        swiping = 1;

                    
                }

                if (swiping === 1) {
                    swiping = pagesMenuWrap.swipeMenu(null, null, 1);
                }

                // UPDATE
                if (command === 'update') {
                    for (var i = 0; i < pages.length; i++) {

                        if ($(pages[i]).hasClass('window-height')) {
                            $(pages[i]).css('margin-top', '-1px').matchWindowHeight();
                        }
                    }
                }
            }
        }
    };

})(jQuery);