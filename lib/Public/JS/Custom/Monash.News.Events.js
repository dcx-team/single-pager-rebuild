/*
 * UTILITIES
 */

/* Resize Windows Handler
 * This uses requestAnimationFrame, which performs better than setTimeOut
 * Source: https://developer.mozilla.org/en-US/docs/Web/Events/resize
 */
var optimizedResize = (function() {
  var callbacks = [],
    running = false;

  // fired on resize event
  function resize() {
    if (!running) {
      running = true;

      if (window.requestAnimationFrame) {
        window.requestAnimationFrame(runCallbacks);
      } else {
        setTimeout(runCallbacks, 66);
      }
    }
  }

  // run the actual callbacks
  function runCallbacks() {
    callbacks.forEach(function(callback) {
      callback();
    });

    running = false;
  }

  // adds callback to loop
  function addCallback(callback) {
    if (callback) {
      callbacks.push(callback);
    }
  }

  return {
    // public method to add additional callback
    add: function(callback) {
      if (!callbacks.length) {
        window.addEventListener("resize", resize);
      }
      addCallback(callback);
    }
  };
})();

document.addEventListener("DOMContentLoaded", function(event) {
  /*
   * START Search Bar Script
   */
  const catCheckboxes = document.querySelectorAll(
    '.search-box--browse__category-list input[type="checkbox"]'
  ); // Get all Checkboxes
  const catNum = catCheckboxes.length; // Get the number of checkboxes (practically number of categories)
  let selectedCats = []; // Array representing the selected categories

  // Initialise the selected categories
  for (let i = 0; i < catCheckboxes.length; i++) {
    handleCheckboxChange(catCheckboxes[i]);
  }

  document
    .querySelector(".search-box--browse__label")
    .addEventListener("click", function(e) {
      document
        .querySelector(".search-box--browse__list")
        .classList.toggle("browse__list-show");
    });

  // Event Listener for "Select All" categories button
  document
    .querySelector(".search-box--browse__select-all")
    .addEventListener("click", function() {
      for (let i = 0; i < catCheckboxes.length; i++) {
        catCheckboxes[i].checked = true;
        handleCheckboxChange(catCheckboxes[i]);
      }
    });

  // Event Listener for "Clear All" categories button
  document
    .querySelector(".search-box--browse__clear-all")
    .addEventListener("click", function() {
      for (let i = 0; i < catCheckboxes.length; i++) {
        catCheckboxes[i].checked = false;
        handleCheckboxChange(catCheckboxes[i]);
      }
    });

  // Event Listener for Search button when all categories are selected
  document
    .querySelector(
      ".search-box--news__button, .search-box--news-events-browse-lhs"
    )
    .addEventListener("click", function() {
      if (selectedCats.length >= catNum) {
        for (let i = 0; i < catCheckboxes.length; i++) {
          catCheckboxes[i].checked = false;
        }
      }
    });

  // Event Listeners for each checkbox
  for (let i = 0; i < catCheckboxes.length; i++) {
    catCheckboxes[i].addEventListener("change", function(e) {
      handleCheckboxChange(e.target);
    });
  }

  // Use to push selected category into SelectedCats array for display
  function handleCheckboxChange(target) {
    if (target.checked) {
      if (selectedCats.indexOf(target.getAttribute("value")) < 0) {
        selectedCats.push(target.getAttribute("value"));
      }
    } else {
      if (selectedCats.indexOf(target.getAttribute("value")) > -1) {
        selectedCats.splice(
          selectedCats.indexOf(target.getAttribute("value")),
          1
        );
      }
    }

    if (selectedCats.length >= catNum || selectedCats.length == 0) {
      document.querySelector(
        ".search-box--browse__selected-categories"
      ).innerHTML = "All Categories";
    } else {
      selectedCats.sort();
      document.querySelector(
        ".search-box--browse__selected-categories"
      ).innerHTML = decodeURIComponent(selectedCats.join(", ")).replace(
        /\+/g,
        " "
      );
    }
  }
  /*
   * END Search Bar Script
   */

  // Even listener for the View More button

  function viewMoreContents() {
    this.remove();
    const loading =
      "<div id='view-more__loading' class='spinner view-more'> <div class='rect'></div><div class='rect'></div><div class='rect'></div><div class='rect'></div><div class='rect'></div> </div>";
    document
      .getElementById("search-news")
      .insertAdjacentHTML("beforeend", loading);

    var moreContents = new XMLHttpRequest();
    moreContents.open("GET", this.getAttribute("data-src"), true);

    moreContents.onload = function() {
      if (this.status >= 200 && this.status < 400) {
        // Success!
        document
          .querySelector("#search-news .box-listing-element__group")
          .insertAdjacentHTML("beforeend", this.responseText);
        const viewMoreBtn = document.querySelector(".btn--view-more");

        if (viewMoreBtn) {
          // View More button listener
          document.getElementById("search-news").appendChild(viewMoreBtn);
          document
            .querySelector(".btn--view-more")
            .addEventListener("click", viewMoreContents);
        }

        document.getElementById("view-more__loading").remove();
      } else {
        console.log("Status" + this.status);
        console.log("Status Text" + this.statusText);
        console.log("Status" + this.statusText);
      }
    };

    moreContents.onerror = function(e) {
      console.log("Error: " + e);
    };

    moreContents.send();
  }

  document
    .querySelector(".btn--view-more")
    .addEventListener("click", viewMoreContents);
});

document.addEventListener("DOMContentLoaded", function(event) {
  /*
   * Frontend Layout Script for News Listing (Currently support only 3-column Layut)
   */
  let newsItems = document.querySelectorAll(
    "#news-events-list .box-listing-element__wrapper"
  );

  let curTop;
  let curLeft;
  let curPos;
  let curListingHeight;
  let autoResize = true;

  const itemWrapOffset = 20;

  console.log(
    "Layout: %globals_asset_metadata_common.news.and.events.layout% - %asset_name%"
  );

  let columnCount;

  if (
    document
      .querySelector("#search-news .box-listing-element__group")
      .classList.contains("box-listing-element__layout-2-col")
  ) {
    columnCount = 2;
  } else {
    columnCount = 3;
  }

  function setNewsItemsSize() {
    if (autoResize) {
      newsItems = document.querySelectorAll(
        "#news-events-list .box-listing-element__wrapper"
      );
      curTop = [0, 0, 0];
      curLeft = 0;
      curListingHeight = 0;
      curPos = 0;

      for (let i = 0; i < newsItems.length; i++) {
        let column = (i + 1) % columnCount; // Used to calculate which column a tile is on. 1 being the right-most column.

        if (column <= 0) {
          column = columnCount - 1; // If the modulus returns 0, it's the right-most column
        } else {
          column--; // If the modulus returns higher than 0, minus 1 to get the correct column count for the array
        }

        // Set the positioning styling of each article
        newsItems[i].setAttribute(
          "style",
          "position: absolute; top: " +
            curTop[column] +
            "px; left: " +
            curLeft +
            "px;"
        );
        curTop[column] += newsItems[i].scrollHeight + itemWrapOffset;

        // Set the position for the next article
        if (column + 1 == columnCount) {
          curLeft = 0;
          curPos = 1;
        } else {
          curLeft += newsItems[i].offsetWidth + itemWrapOffset;
          curPos++;
        }
      }

      for (let i = 0; i < curTop.length; i++) {
        if (curTop[i] > curListingHeight) {
          curListingHeight = curTop[i];
        }
      }

      curListingHeight += 10;

      // Add View More button's height if it exists
      if (document.querySelector(".btn--view-more")) {
        curListingHeight += document.querySelector(".btn--view-more")
          .scrollHeight;
      }

      document
        .getElementById("news-events-list")
        .setAttribute("style", "height: " + curListingHeight + "px");
    }
  }

  setNewsItemsSize();

  // start process
  optimizedResize.add(function() {
    setNewsItemsSize();
  });
  /* END Resize Windows Handler */

  /* JS Media Query*/
  function newsEventsPageHandler(mediaQuery) {
    if (mediaQuery.matches) {
      // If media query matches
      autoResize = false;
      for (let i = 0; i < newsItems.length; i++) {
        newsItems[i].removeAttribute("style");
      }
      document.getElementById("news-events-list").removeAttribute("style");
    } else {
      autoResize = true;
    }
  }

  var neMediaQuery = window.matchMedia("(max-width: 736px)");
  newsEventsPageHandler(neMediaQuery); // Call listener function at run time
  neMediaQuery.addListener(newsEventsPageHandler); // Attach listener function on state changes

  /*
   * END Frontend Layout Script for News Listing (Currently support only 3-column Layut)
   */

  /*
   * General Functions
   */

  /* Observing DOM Change to resize with contents*/
  // Select the node that will be observed for mutations
  var targetNode = document.getElementById("news-events-list");

  // Options for the observer (which mutations to observe)
  var config = { attributes: false, childList: true, subtree: true };

  // Create an observer instance linked to the callback function
  var observer = new MutationObserver(function() {
    setNewsItemsSize();
    var allImages = document.querySelectorAll("img:not([data-loaded]");
    for (var i = 0; i < allImages.length; i++) {
      if (allImages[i].complete) {
        allImages[i].setAttribute("data-loaded", "true");
      } else {
        allImages[i].addEventListener("load", function(event) {
          this.setAttribute("data-loaded", "true");
          setNewsItemsSize();
        });
      }
    }
  });

  // Start observing the target node for configured mutations
  observer.observe(targetNode, config);
});
