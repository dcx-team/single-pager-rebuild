/**
 *
 *
 * Monash Gallery
 * jeremy.hewitt@monash.edu
 * 2016
 *
 *
 **/
;

$(document).ready(function() {
    $('body').gallery('init');
});

(function($) {

    var zz = $('<i/>'),
        feed = "https://www.monash.edu/_core/layout/json/gallery/feed/_nocache",
        gallery;

    var methods = {

        init: function() {
            $('.gallery-item').click(function(e) {
                var root = $(this).closest('.asset-list').attr('data-asset'),
                    id = $(this).closest('.asset').attr('data-asset');

                // $(this).gallery("launch",id,root);

                e.preventDefault();
                return false;
            })
        },

        launch: function(id, root) {
            gallery = zz.element("div", "asset fixed top right bottom left top-layer black-background");

            var tools = zz.element("div", "absolute top right top-layer larger"),
                closeBtn = zz.element('a', 'inline-block pointer white mobile-large middle relative large').attr('href', '#').append(zz.closeBtn()),
                fullScreenBtn = zz.element('a', 'inline-block pointer white middle small expand-button').attr('href', '#').append(zz.icon('expand')),
                inner = zz.element("div", "inner full-width full-height full-width-sly"),
                list = zz.element("ul", "no-list no-wrap full-width full-height"),
                items = [],

                loaderDiv = zz.element("div", "absolute always-middle-of-parent white larger"),
                loaderIcon = zz.icon("spin fa-circle-o-notch"),

                thumbsContainer = zz.element("div", "absolute top-layer right bottom left transition hover-ui no-mobile"),
                thumbsList = zz.element("div", "pages-nav no-wrap").css("overflow-x", "auto"),
                thumbs = [],

                navigation = '<div class="fixed top bottom left width-5 desktop-only layer-1 sly-left-controls transition"> <div class="relative full-width full-height"> <a href="#" class="sly-prev absolute trans-black-background white padding vertical-padding block full-width always-middle-of-parent largerer center" title="Slide left" aria-controls="gallery-%asset_assetid%"> <i class="fa fa-caret-left"></i> </a> </div></div><div class="fixed top bottom right width-5 desktop-only layer-1 sly-right-controls transition"> <div class="relative full-width full-height"> <a href="#" class="sly-next absolute trans-black-background white padding vertical-padding block full-width always-middle-of-parent largerer center" title="Slide right" aria-controls="gallery-%asset_assetid%"> <i class="fa fa-caret-right"></i> </a> </div></div>',

                startAt = 0;

            var payload = {
                "root": root
            }

            $.ajax({
                "type": "GET",
                "data": payload,
                "dataType": "json",
                "url": feed,
                "success": function(data) {
                    if (!data.error) {

                        $.each(data.results, function(o, v) {

                            if (v.id === id) {
                                startAt = parseFloat(o) - 1;
                                console.log(startAt);
                            }

                            var title = v.details.title.substring(0, 20),
                                titleIcon = zz.icon("camera"),
                                titlePara = zz.element("p", "no-margin no-padding"),
                                caption = v.details.caption,
                                captionElement = zz.element("span", "block"),
                                captionHeightBuffer = zz.element("div", "row-list-thumbnail no-mobile").append(zz.element("div", "small-padding"));

                            if (zz.checkVariable(caption)) {} else {
                                caption = "No caption";
                            }

                            captionElement.html(caption);

                            if (v.details.title.length > 20) {
                                title = " " + title + "...";
                            }

                            title = zz.element("span", "inline-block").html(title);

                            var item = zz.element("li", "inline-block wrap full-width full-height relative no-overflow"),
                                image = zz.element("div", "background-image constrain full-width full-height relative"),
                                titleBar = zz.element("div", "trans-black-background white padding absolute top right left no-wrap center").append(
                                    titlePara.append(titleIcon).append(title)
                                ),
                                captionBar = zz.element("div", "trans-black-background white padding absolute right bottom left hover-ui transition").append(captionElement).append(captionHeightBuffer);

                            image.css({
                                "background-image": "url(" + v.varieties.large + ")"
                            });

                            item.append(
                                image
                            ).append(
                                titleBar
                            ).append(
                                captionBar
                            );

                            items.push(item);

                            thumbs.push(v.varieties.small);
                        });

                        list.append(items);

                        thumbsContainer.append(
                            thumbsList
                        );

                        inner.append(navigation);
                        inner.middleOfParent();

                        inner.swipeMenu(null, startAt, null, thumbs);
                    } else {
                        gallery.remove();
                    }
                },
                "error": function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });

            $("body").append(
                gallery.append(
                    loaderDiv.append(
                        loaderIcon
                    )
                ).append(
                    inner.append(
                        list
                    ).append(
                        tools.append(
                            fullScreenBtn
                        ).append(
                            closeBtn
                        )
                    ).append(
                        thumbsContainer
                    )
                )
            );

            inner.middleOfParent();

            closeBtn.click(function(e) {
                gallery.gallery("close");
                e.preventDefault();
            });

            fullScreenBtn.click(function(e) {
                $(document).toggleFullScreen();
                $(this).find('i.fa').toggleClass('fa-compress');
                e.preventDefault();
            });

            $('body').bind({
                "keyup": function(e) {
                    var code = e.keyCode || e.which;

                    // Escape
                    if (code === 27) {
                        gallery.gallery("close");
                        $('body').unbind("keyup");
                    }

                    // Left
                    if (code === 37) {
                        gallery.find('a.sly-prev').click();
                    }

                    // Right
                    if (code === 39) {
                        gallery.find('a.sly-next').click();
                    }
                }
            });

            gallery.mousemove(function() {
                gallery.addClass('hovering');
                typeDelay(function() {
                    gallery.removeClass('hovering');
                }, 3000);
            });

            // $('body').toggleFullScreen();
        },

        close: function() {
            $(this).remove();
            $('body').unbind("keyup");
            // $('body').toggleFullScreen();
        }

    };

    $.fn.gallery = function(methodOrOptions) {
        if (methods[methodOrOptions]) {
            return methods[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            // Default to "init"
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.gallery');
        }
    };

})(jQuery);