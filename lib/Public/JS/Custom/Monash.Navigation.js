$(document).ready(function() {
  (function checkPageType() {
    const isOnePager = $(".one-pager .js-hamburgerNav").length;
    const isMultipage = $(".multi-page").length;

    if (isOnePager) {
      $(".one-pager .js-hamburgerNav")
        .removeClass("visually-hidden")
        .attr("aria-hidden", "false");

      initSPHamburgerMenu();
    } else if (isMultipage) {
      $("#multipage, #menu-toggle")
        .removeClass("visually-hidden")
        .attr("aria-hidden", "false");
      initMPHamburgerMenu();
    }
  })();

  $(".one-pager-wrapper").each(function() {
    var target = $(this);

    target.onePager("init");

    $(window).scroll();

    $(window).resize(function() {
      target.onePager("update");
    });
  });

  // Link CTA buttons to next section if empty
  $(".one-pager-next-link").click(function() {
    var headerHeight = $("header").outerHeight();
    var scrollTo = $(this)
      .parent()
      .find(".asset")
      .next();
    $("html, body").animate(
      {
        scrollTop: $(this).offset().top + headerHeight
      },
      1000,
      "easeOutQuart"
    );
  });

  // get section names and store in an array
  var sectionNames = $(".one-pager-wrapper div.asset")
    .map(function() {
      return $(this).data("name");
    })
    .get();

  // get section ID and store in an array
  var sectionID = $(".one-pager-wrapper div.asset")
    .map(function() {
      return this.id;
    })
    .get();

  // generate one-pager top navigation from sectionNames()
  // check if one-pager is using hamburger navigation
  if ($(".one-pager .js-hamburgerNav").length) {
    $("#main-navigation-sidemenu").addClass(
      "relative full-width transition transparent no-background"
    );
    var menuList = $(".one-pager .js-hamburgerNav");
    menuList.addClass("no-list");
    $("#menu-toggle").removeClass("visually-hidden");
    $.each(sectionNames, function(i) {
      var liMenu = $("<li/>")
        .addClass("asset inline-block")
        .appendTo(menuList);
      var aMenu = $("<a/>")
        .addClass(
          "block padding mobile-large-padding black no-underline white-background"
        )
        .attr("role", "tab")
        .attr("href", "#" + sectionID[i])
        .appendTo(liMenu);
      var spanMenu = $("<span/>")
        .addClass("generated-ui undefined")
        .text(sectionNames[i])
        .appendTo(aMenu);
      var hoverMenu = $("<span/>")
        .addClass(
          "generated-ui menu-active-item-marker absolute bottom right left margin-auto transition width-0"
        )
        .appendTo(aMenu);
    });
  } else if ($(".one-pager .js-sp-main-navigation").length) {
    // standard SP nav
    $("#main-navigation").addClass(
      "relative sticky-nav-tracks js-one-pager-nav full-width transition transparent no-background"
    );
    var menuList = $(".one-pager .js-sp-main-navigation");
    menuList.addClass("generated-ui no-list list-menu no-wrap one-pager-menu");
    $("#menu-toggle").hide();
    $.each(sectionNames, function(i) {
      var liMenu = $("<li/>")
        .addClass("generated-ui list-menu-item")
        .appendTo(menuList);
      var aMenu = $("<a/>")
        .addClass(
          "generated-ui block large no-underline short-transition relative"
        )
        .attr("role", "tab")
        .attr("href", "#" + sectionID[i])
        .appendTo(liMenu);
      var spanMenu = $("<span/>")
        .addClass("generated-ui undefined")
        .text(sectionNames[i])
        .appendTo(aMenu);
      var hoverMenu = $("<span/>")
        .addClass(
          "generated-ui menu-active-item-marker absolute bottom right left margin-auto transition width-0"
        )
        .appendTo(aMenu);
    });
  } else if ($(".one-pager .js-sp-main-navigation").length) {
    // standard SP nav
    $("#main-navigation").addClass(
      "relative sticky-nav-tracks js-one-pager-nav full-width transition transparent no-background"
    );
    var menuList = $(".one-pager .js-sp-main-navigation");
    menuList.addClass("generated-ui no-list list-menu no-wrap one-pager-menu");
    $("#menu-toggle").hide();
    $.each(sectionNames, function(i) {
      var liMenu = $("<li/>")
        .addClass("generated-ui list-menu-item")
        .appendTo(menuList);
      var aMenu = $("<a/>")
        .addClass(
          "generated-ui block large no-underline short-transition relative"
        )
        .attr("role", "tab")
        .attr("href", "#" + sectionID[i])
        .appendTo(liMenu);
      var spanMenu = $("<span/>")
        .addClass("generated-ui undefined")
        .text(sectionNames[i])
        .appendTo(aMenu);
      var hoverMenu = $("<span/>")
        .addClass(
          "generated-ui menu-active-item-marker absolute bottom right left margin-auto transition width-0"
        )
        .appendTo(aMenu);
    });
  }

  // Add a gradient to the navigation when window is less and nav width
  function addNavHelper() {
    var windowWidth = $(window).width();
    var navWidth = $("#main-navigation .js-sp-main-navigation").innerWidth();
    if ($(window).width() > navWidth) {
      $(".swipe-menu-helper").hide();
      $(".js-sp-main-navigation").css("overflow-x", "visible");
    } else {
      $(".swipe-menu-helper").show();
      $(".js-sp-main-navigation").css("overflow-x", "scroll");
    }
  }
  addNavHelper();

  // Breaks smool scroll to section
  // // Toggle nav on click
  // $('#main-navigation .js-hamburgerNav > li > a').click(function(){
  //   initSPHamburgerMenu().nav.close();
  // });
  function removeHidden() {
    $("#side-menu-overlay").toggleClass("hidden");
  }

  const checkNav = $("#main-navigation-sidemenu").length;
  if (checkNav) {
    initHamburgerNav();
    var newNav = responsiveNav("#main-navigation-sidemenu", {
      animate: true,
      transition: 254,
      open: function() {
        removeHidden();
        $("#main-navigation-sidemenu").addClass("opaque");
      }, // Function: Open callback
      close: function() {
        removeHidden();
        $("#main-navigation-sidemenu").removeClass("opaque");
      } // Function: Close callback
    });
  }

  $(window).resize(throttle(addNavHelper, 1000));
  $(window).scroll(function() {
    $("#main-navigation li > .active")
      .parent()
      .css("left", -$(window).scrollLeft());
  });
  // http://codepen.io/chriscoyier/pen/dpBMVP
  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000,
            function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();

              if ($target.is(":focus")) {
                // Checking if the target was focused

                return false;
              } else {
                $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
            }
          );
        }
      }
    });

  // throttle function, enforces a minimum time interval
  function throttle(fn, interval) {
    var lastCall, timeoutId;
    return function() {
      var now = new Date().getTime();
      if (lastCall && now < lastCall + interval) {
        // if we are inside the interval we wait
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
          lastCall = now;
          fn.call();
        }, interval - (now - lastCall));
        return "aborted";
      } else {
        // otherwise, we directly call the function
        lastCall = now;
        fn.call();
      }
    };
  }
  /**
   *  Generate one pager navigation links from section IDs
   */
  // Cache selectors

  var $navigationLinks = $("#main-navigation .js-sp-main-navigation > li > a");
  var $hamburgerNavigationLinks = $(
    "#main-navigation-sidemenu .js-hamburgerNav > li > a"
  );

  var $sections = $(".page-wrapper div.asset");
  var $sectionsReversed = $(
    $(".page-wrapper div.asset")
      .get()
      .reverse()
  );
  var sectionIdTonavigationLink = {};
  var hamburgerSectionIdTonavigationLink = {};

  $sections.each(function() {
    sectionIdTonavigationLink[$(this).attr("id")] = $(
      '#main-navigation .js-sp-main-navigation > li > a[href="#' +
        $(this).attr("id") +
        '"]'
    );
  });

  $sections.each(function() {
    hamburgerSectionIdTonavigationLink[$(this).attr("id")] = $(
      '#main-navigation-sidemenu .js-hamburgerNav > li > a[href="#' +
        $(this).attr("id") +
        '"]'
    );
  });

  // Highlight current section if in scroll position
  // This is applied for single pager sites

  function scrollToSection() {
    var scrollPosition = $(window).scrollTop();
    var headerHeight = $("header").outerHeight();

    $sectionsReversed.each(function() {
      var currentSection = $(this);
      var sectionTop = currentSection.offset().top;
      if (scrollPosition >= sectionTop - headerHeight) {
        var id = currentSection.attr("id");
        var $navigationLink = sectionIdTonavigationLink[id];
        var $hamburgerNavigationLink = hamburgerSectionIdTonavigationLink[id];

        if (!$navigationLink.parent().hasClass("active")) {
          $navigationLinks.parent().removeClass("active");
          $hamburgerNavigationLinks
            .removeClass("blue-background active")
            .addClass("white-background");

          $navigationLink
            .parent()
            .addClass("active")
            .focus();
          $hamburgerNavigationLink
            .addClass("blue-background active")
            .removeClass("white-background");
        }

        return false;
      }
    });
  }

  function hideNav() {
    var winScroll =
      $(window).scrollTop() + $(".header-height-buffer").outerHeight();

    if (winScroll > 150) {
      $("#main-navigation-sidemenu, #main-navigation").addClass("opaque");
    } else {
      $("#main-navigation-sidemenu, #main-navigation").removeClass("opaque");
    }
  }

  $(window).scroll(throttle(scrollToSection, 500));
  $(window).scroll(throttle(hideNav, 1000));

  // Call Sly on frame
  (function() {
    var $frame = $(".js-one-pager-nav");
    var $wrap = $frame.parent();
    $frame.sly({
      horizontal: 1,
      itemNav: "basic",
      smart: 1,
      activateOn: "click",
      mouseDragging: 1,
      touchDragging: 1,
      releaseSwing: 1,
      startAt: 3,
      scrollBar: $wrap.find(".scrollbar"),
      scrollBy: 1,
      pagesBar: $wrap.find(".pages"),
      activatePageOn: "click",
      speed: 300,
      elasticBounds: 1,
      easing: "easeOutExpo",
      dragHandle: 1,
      dynamicHandle: 1,
      clickBar: 1
    });
  })();
  // throttle(highlightNavSection, 100 );
  // TO DO
  // Add conditonal to check multi or single page
  function initSPHamburgerMenu() {
    var nav = newNav;
  }

  function initMPHamburgerMenu() {
    var nav = responsiveNav("multipage", {
      // Selector
      animate: true, // Boolean: Use CSS3 transitions, true or false
      transition: 284, // Integer: Speed of the transition, in milliseconds
      label: "Menu", // String: Label for the navigation toggle
      insert: "before", // String: Insert the toggle before or after the navigation
      customToggle: "#menu-toggle", // Selector: Specify the ID of a custom toggle
      closeOnNavClick: true, // Boolean: Close the navigation when one of the links are clicked
      openPos: "relative", // String: Position of the opened nav, relative or static
      navClass: "nav-collapse", // String: Default CSS class. If changed, you need to edit the CSS too!
      navActiveClass: "js-nav-active", // String: Class that is added to  element when nav is active
      jsClass: "js", // String: 'JS enabled' class which is added to  element
      init: function() {}, // Function: Init callback
      open: function() {}, // Function: Open callback
      close: function() {} // Function: Close callback
    });
  }

  // Display hamburger nav if number of menu items exceeds width of header
  function initHamburgerNav() {
    var widths = $.map(
      $("#main-navigation-sidemenu > ul > li, #multipage > ul > li"),
      function(e) {
        return $(e).width();
      }
    );
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    let navWidthLi = widths.reduce(reducer);
    let windowWidth = $(window).width();
    let headerWidth = $("header .master-wrap").innerWidth();
    let actualNavWidth = $("#main-navigation-sidemenu").outerWidth();
    let navWidth = actualNavWidth;

    if (headerWidth <= navWidthLi && windowWidth > 600) {
      $("#main-navigation-sidemenu > ul, #multipage > ul").addClass(
        "max-width-50"
      );
      $("#main-navigation-sidemenu > ul > li, #multipage > ul > li").addClass(
        "nav-li-block"
      );
      $("#menu-toggle").css("display", "block");
      $(".menu-active-item-marker.absolute").css("display", "none");
      $("#menu-toggle").click(function() {
        $("#side-menu-overlay").toggleClass("hidden");
      });
    }
  }
});
