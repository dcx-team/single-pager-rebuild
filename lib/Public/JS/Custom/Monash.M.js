$(document).ready(function(){
    $('.big-m').each(function(){
        $(this).monashM({
            'sides': true,
            'style': $(this).attr('data-style')
        });
    });
});


(function($){
    
    var zz = $(document);
    
    $.fn.monashM = function(options){
        
        var settings = $.extend({
            "style": "black",
            "animation": null,
            "sides": false
        },options),
            
            target = this,
            
            ratio = 2.15,

            monashM = $('<div/>').attr('class','monash-m absolute top bottom transition'),
            topWedge = $('<div/>').attr('class',settings.style+' monash-m--wedge monash-m--top-wedge background-image absolute top'),
            bottomLeftWedge = $('<div/>').attr('class',settings.style+' monash-m--wedge monash-m--bottom-left-wedge background-image absolute bottom'),
            bottomRightWedge = $('<div/>').attr('class',settings.style+' monash-m--wedge monash-m--bottom-right-wedge background-image absolute bottom'),
        
            height = target.outerHeight(),
            width = height/ratio,
            
            items = [];
        
        if(width>=$(window).width()*.29){
            width = $(window).width()*.29;
        }
        
        if(!target.hasClass('big-m-loaded')){
            
            target.addClass('big-m-loaded');
        
            monashM.css('width',width);

            monashM.append(
                topWedge
            ).append(
                bottomLeftWedge
            ).append(
                bottomRightWedge
            );

            target.append(monashM);

            items.push(topWedge,bottomLeftWedge,bottomRightWedge);

            if(settings.sides===true){

                var targetWidth = target.outerWidth(),
                    masterWrap = $('<div/>').attr('class','master-wrap');

                target.append(masterWrap);

                var masterWrapWidth = masterWrap.outerWidth(),
                    leftWidth = (targetWidth-masterWrapWidth)/2,
                    mWidth = width,
                    rightWidth = (targetWidth-width)-leftWidth,
                    rightOffset = width+leftWidth,
                    contentOffset = width;
                /*
                if($(window).width()<=720){
                    leftWidth = '30%';
                    rightWidth = '30%';
                    mWidth = '40%';
                    rightOffset = '70%';
                    contentOffset = '0%';
                }
                
                if($(window).width()<=609){
                    leftWidth = '20%';
                    rightWidth = '20%';
                    mWidth = '60%';
                    rightOffset = '80%';
                    contentOffset = '0%';
                }
                
                if($(window).width()<=480){
                    leftWidth = '12%';
                    rightWidth = '12%';
                    mWidth = '76%';
                    rightOffset = '88%';
                    contentOffset = '0%';
                }
                */
                
                monashM.css('width',mWidth);

                var leftBlock = $('<div/>').attr('class',settings.style+'-background absolute top bottom transition').css('width',leftWidth),
                    rightBlock = $('<div/>').attr('class',settings.style+'-background absolute top right bottom transition').css({
                        // 'width': rightWidth,
                        'left': rightOffset
                    });

                monashM.css('left',leftWidth);

                target.prepend(leftBlock);
                target.append(rightBlock);

                leftBlock.css({
                    'opacity': 0,
                    'transform': 'translate3d(-700px,0px,0px)'
                });

                rightBlock.css({
                    'opacity': 0,
                    'transform': 'translate3d(700px,0px,0px)'
                });

                items.push(leftBlock,rightBlock);

                var container = target.closest('.big-m-container');
                if(container.length>0){

                    var content = container.find('.big-m-offset');
                    content.css('margin-left',contentOffset);
                }

            }

            topWedge.css({
                'opacity': 0,
                'transform': 'translate3d(0px,-700px,0px)'
            });
            bottomLeftWedge.css({
                'opacity': 0,
                'transform': 'translate3d(0px,700px,0px)'
            });
            bottomRightWedge.css({
                'opacity': 0,
                'transform': 'translate3d(0px,700px,0px)'
            });

            setTimeout(function(){
                for(var i = 0; i < items.length; i++){
                    for(var i = 0; i < items.length; i++){
                        $(items[i]).addClass('long-transition');
                    }
                }
            },100);

            setTimeout(function(){
                for(var i = 0; i < items.length; i++){
                    $(items[i]).css({
                        'opacity': 1,
                        'transform': 'translate3d(0px,0px,0px)'
                    });
                }
            },200);

            $(window).resize(function(){

                for(var i = 0; i < items.length; i++){
                    $(items[i]).removeClass('long-transition');
                }

                height = target.outerHeight(),
                width = height/ratio;
                
                if(width>=$(window).width()*.29){
                    width = $(window).width()*.29;
                }
                
                mWidth = width;
                targetWidth = target.outerWidth();
                masterWrapWidth = masterWrap.outerWidth();
                leftWidth = (targetWidth-masterWrapWidth)/2;
                rightWidth = (targetWidth-width)-leftWidth;
                rightOffset = width+leftWidth;
                contentOffset = width;
/*
                if($(window).width()<=720){
                    leftWidth = '30%';
                    rightWidth = '30%';
                    mWidth = '40%';
                    rightOffset = '70%';
                    contentOffset = '0%';
                }
                
                if($(window).width()<=609){
                    leftWidth = '20%';
                    rightWidth = '20%';
                    mWidth = '60%';
                    rightOffset = '80%';
                    contentOffset = '0%';
                }

                if($(window).width()<=480){
                    leftWidth = '12%';
                    rightWidth = '12%';
                    mWidth = '76%';
                    rightOffset = '88%';
                    contentOffset = '0%';
                }
*/
                content.css('margin-left',contentOffset);

                monashM.css({
                    'width': mWidth,
                    'left': leftWidth
                });

                leftBlock.css('width',leftWidth);
                rightBlock.css({
                //    'width': rightWidth,
                    'left': rightOffset
                });
            });
            
        }
        
    };
    
})(jQuery);