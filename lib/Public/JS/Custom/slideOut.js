let windowWidth = $(window).width();
let slideoutWidth = windowWidth - 60;

if (windowWidth > 600) {
  $("#slider").slideReveal({
    trigger: $("#trigger"),
    position: "right",
    width: slideoutWidth
  });

  $("#trigger").click(function() {
    $(this).toggleClass("active");
    $(".slide-reveal-overlay").hide();
    $(".js-enquire").toggle();
    $(this).toggleClass("js-trigger--active");
  });
} else {
  $("#slider").slideReveal({
    trigger: $("#trigger"),
    top: 50,
    position: "left",
    width: slideoutWidth
  });

  $("#trigger").click(function() {
    $(this).toggleClass("active");
    $(".slide-reveal-overlay").hide();
    $(".js-enquire").toggle();
    $(this).toggleClass("js-trigger--active");
  });
}
