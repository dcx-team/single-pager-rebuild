/*!
 * gulp
 * $ npm install gulp-ruby-sass gulp-autoprefixer gulp-plumber gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-browser-sync gulp-useref run-sequence gulp-if gulp-file-include' gulp-cache del --save-dev
 */

// Load plugins
var gulp = require("gulp"),
  sass = require("gulp-ruby-sass"),
  plumber = require("gulp-plumber"),
  autoprefixer = require("gulp-autoprefixer"),
  cssnano = require("gulp-cssnano"),
  uglify = require("gulp-uglify"),
  imagemin = require("gulp-imagemin"),
  rename = require("gulp-rename"),
  concat = require("gulp-concat"),
  notify = require("gulp-notify"),
  cache = require("gulp-cache"),
  livereload = require("gulp-livereload"),
  del = require("del"),
  browserSync = require("browser-sync").create(),
  useref = require("gulp-useref"),
  runSequence = require("run-sequence"),
  gulpIf = require("gulp-if"),
  rename = require("gulp-rename"),
  fileinclude = require("gulp-file-include"),
  ignoreErrors = require("gulp-ignore-errors"),
  sourcemaps = require("gulp-sourcemaps"),
  babel = require("gulp-babel"),
  gutil = require("gulp-util");

gulp.task("fileinclude", function() {
  gulp
    .src(["src/index.html"])
    .pipe(
      fileinclude({
        prefix: "@@",
        basepath: "@file"
      })
    )
    .pipe(gulp.dest("public/"));
});

//script paths
var editJSFiles = "lib/Edit/JS/*.js",
  editJSPlugins = "lib/Edit/Plugins/**/*.js",
  customJSFiles = "lib/Public/JS/Custom/**/*.js",
  pluginJSFiles = "lib/Public/JS/Plugins/**/*.js",
  jQuery = "lib/Public/JS/jQuery/jQuery.js",
  jQueryUI = "lib/Public/JS/jQuery.UI/jQuery.UI.js",
  jsDeploy = "bitbucket/minified/javascript",
  jsBuild = "bitbucket/unminified/javascript",
  jsDev = "lib/all/edit",
  jsPublic = "lib/Public/build/JS",
  jsPlugins = "lib/all/plugins",
  jsPluginsCompressed = "bitbucket/minified/javascript/plugins.min.js",
  cssOrder = [
    "lib/Public/CSS/font-awesome.min.css",
    "lib/Public/CSS/Monash.Bootstrap.css",
    "lib/Public/CSS/Monash.Forms.css",
    "lib/Public/CSS/Monash.Graphics.css",
    "lib/Public/CSS/MonashTemplates.css",
    "lib/Public/CSS/Monash.News.Events.css",
    "lib/Public/CSS/responsive-nav.css",
    "lib/Public/CSS/slideOut.css"
  ],
  jsOrder = [
    "lib/Public/JS/jQuery/jQuery.js",
    "lib/Public/JS/Plugins/jQuery.Easing/jQueryeasing.js",
    "lib/Public/JS/Plugins/WebFont/webfont.js",
    "lib/Public/JS/Plugins/Sly/sly.min.js",
    "lib/Public/JS/Custom/behavs.js",
    "lib/Public/JS/Custom/Monash.Gallery.js",
    "lib/Public/JS/Custom/Monash.M.js",
    "lib/Public/JS/Custom/Monash.Navigation.js",
    "lib/Public/JS/Plugins/responsive-nav.js",
    "lib/Public/JS/Custom/Monash.Templates.OnePager.js",
    "lib/Public/JS/Plugins/slidereveal.js",
    "lib/Public/JS/Plugins/enquireNow.js",
    "lib/Public/JS/Custom/slideOut.js",
    "lib/Public/JS/Custom/Monash.News.Events.js"
  ];
(editCSSFiles = ["lib/Edit/CSS/*.css", "lib/Edit/Plugins/**/*.css"]),
  (publicCSSFiles = ["lib/Public/CSS/*.css"]);

gulp.task("generateAllScripts", function() {
  return [
    gulp
      .src([jQuery, pluginJSFiles, customJSFiles])
      .pipe(plumber({ errorHandler: onError }))
      .pipe(concat("public.js"))
      .pipe(gulp.dest(jsPublic))
      .pipe(rename("public.min.js"))
      .pipe(uglify())
      .pipe(gulp.dest(jsDeploy))
  ];
});

gulp.task("generateAllStyles", function() {
  return [
    /* gulp.src(editCSSFiles)
             .pipe(cssnano({
                 reduceIdents: {
                     keyframes: false
                 },
                 discardUnused: {
                     keyframes: false
                 },
                 zindex: false
             }))
             .pipe(concat('edit.min.css'))
             .pipe(gulp.dest('bitbucket/minified/stylesheets/')),
         */
    gulp
      .src(publicCSSFiles)
      .pipe(
        cssnano({
          reduceIdents: {
            keyframes: false
          },
          discardUnused: {
            keyframes: false
          },
          zindex: false
        })
      )
      .pipe(concat("public.min.css"))
      .pipe(gulp.dest("bitbucket/minified/stylesheets/"))
  ];
});

// Scripts
gulp.task("scripts", function() {
  return (
    gulp
      .src("src/javascripts/modules/**/*.js")
      .pipe(plumber({ errorHandler: onError }))
      //.pipe(jshint('.jshintrc'))
      //.pipe(jshint.reporter('default'))
      .pipe(concat("main.js"))
      .pipe(gulp.dest("public/scripts"))
      .pipe(rename({ suffix: ".min" }))
      .pipe(uglify())
      .pipe(gulp.dest("public/scripts"))
      .pipe(notify({ message: "Scripts task complete" }))
  );
});

// Images
gulp.task("images", function() {
  return gulp
    .src("src/images/**/*")
    .pipe(
      cache(
        imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })
      )
    )
    .pipe(gulp.dest("public/images"))
    .pipe(notify({ message: "Images task complete" }));
});

gulp.task("browserSync", function() {
  browserSync.init({
    server: {
      baseDir: "lib/Public"
    }
  });
});

gulp.task("css", function() {
  return gulp
    .src(cssOrder)
    .pipe(
      cssnano({
        reduceIdents: {
          keyframes: false
        },
        discardUnused: {
          keyframes: false
        }
      })
    )
    .pipe(sourcemaps.init())
    .pipe(concat("public.min.css"))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("bitbucket/minified/stylesheets/"))
    .pipe(gulp.dest("lib/Public/build/CSS/"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
});
// Scripts
gulp.task("js", function() {
  return gulp
    .src(jsOrder)
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(concat("public.js"))
    .pipe(gulp.dest(jsPublic))
    .pipe(rename("public.min.js"))
    .pipe(uglify())
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(jsDeploy));
});

// create a task that ensures the `js` task is complete before
// reloading browsers

gulp.task("js-watch", ["js", "css"], function(done) {
  browserSync.reload();
  done();
});

// Build all scripts and stylesheets to the bitbucket dir
gulp.task("build", ["js", "css"]),
  function(done) {
    done();
  };

gulp.task("fileinclude", function() {
  gulp
    .src([
      "lib/Public/index.html",
      "lib/Public/spwithhamburger.html",
      "lib/Public/multipage.html"
    ])
    .pipe(
      fileinclude({
        prefix: "@@",
        basepath: "@file"
      })
    )
    .pipe(gulp.dest("./lib/Public/build"))
    .pipe(browserSync.reload({ stream: true })); // reload after compilation
});

gulp.task("browserSync", function() {
  browserSync.init({
    server: {
      baseDir: "lib/Public/",
      index: "./build/index.html"
    }
  });
});

// Static Server + watching scss/html files
gulp.task("default", ["fileinclude", "css", "js", "browserSync"], function() {
  // Watch .scss files
  gulp.watch("lib/Public/CSS/**/*.css", ["css"]);
  gulp.watch("lib/Public/JS/**/*.js", ["js"]);
  gulp.watch("lib/Public/**/*.html", ["fileinclude"]);
});

/* -------------------------------------------------------------------------------------------------
Utilities Tasks
-------------------------------------------------------------------------------------------------- */
var onError = function(err) {
  gutil.beep();
  gutil.log(errorMsg + " " + err.toString());
  this.emit("end");
};

var errorMsg = "\x1b[41mError\x1b[0m";
